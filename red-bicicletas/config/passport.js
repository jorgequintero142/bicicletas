var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var Usuario = require('../models/usuario');



passport.use(new LocalStrategy({
    usernameField: 'mail'
  },
  function(mail, password, done) {

    Usuario.findOne({ email: mail  }, function (err, user) {
		
      if (!user) {
		  console.log("No se ha encontrado el usuario");
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
		   console.log("Contraseña no es valida");
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  Usuario.findById(id, function(err, user) {
    done(err, user);
  });
});

module.exports = passport;