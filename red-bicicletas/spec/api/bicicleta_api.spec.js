var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
beforeEach(()=>{
		Bicicleta.allBicis = [];
});


describe('Bicicletas API', ()=>{
	describe('GET Bicicletas/',()=>{
		it('Status 200',(done)=>{
			
			expect(Bicicleta.allBicis.length).toBe(0);
			var bici = new Bicicleta();
			Bicicleta.add(bici);
			request.get('http://127.0.0.1:3000/api/bicicletas',function(error, response, body){
				
				expect(response.statusCode).toBe(200);
				done();				
			});
		});
		
	});
	
	
	describe('POST Bicicletas/create',()=>{
		it('Status 200',(done)=>{
			var headers = {'content-type':'application/json'};
			var aBici = '{"id":"1","color":"morado","modelo: ":"clasica"}';


			request.post({
				headers: headers,
				url:'http://127.0.0.1:3000/api/bicicletas/create',
				body: aBici				
			}, function(error, response, body) {
				
				
				expect(response.statusCode).toBe(200);
				expect(Bicicleta.findById(1).color).toBe("morado");
				done();
				
			}); 
		});
		
	});
	
	
	
	describe('POST Bicicletas/delete',()=>{
		it('Status 204',(done)=>{
			var headers = {'content-type':'application/json'};
			
			var bici = new Bicicleta(1, "verde","bmx");
			Bicicleta.add(bici);
			expect(Bicicleta.allBicis.length).toBe(1);
			expect(Bicicleta.allBicis[0].color).toBe("verde");
			var dBici = '{"id":"1"}';


			request.post({
				headers: headers,
				url:'http://127.0.0.1:3000/api/bicicletas/delete',
				body: dBici				
			}, function(error, response, body) {				
				expect(response.statusCode).toBe(204);
				expect(Bicicleta.allBicis.length).toBe(0);
				done();
				
			}); 
		});
		
	});	
});