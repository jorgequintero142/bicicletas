var mongoose = require("mongoose");
var Bicicleta = require('../../models/bicicleta');

describe('Test Bicicletas',()=>{
	beforeEach(function(done){
		var mongoose = require('mongoose');
		mongoose.connect('mongodb://127.0.0.1:27017/red_bicicletas');
		var db = mongoose.connection;
		db.on('error test connect', console.error.bind(console, 'connection error:'));
		db.once('open', function() {
		console.log('Ok connect test ');
		done();
		}); 
	});	
	
	afterEach(function(done){
		Bicicleta.deleteMany({},function(err,success){
			if (err) console.log(err);
			done();
		});		
	});
	
	describe('Bicicleta.createInstance',()=>{
	it('crear instancia', ()=>{
		
		var bici = Bicicleta.createInstance(1,"verde","de calle",[-22.9, -43.2]);
		
		
		expect(bici.code).toBe(1);
		expect(bici.color).toBe("verde");
		expect(bici.modelo).toBe("de calle");
	});	
});
	
	
});


/*
beforeEach(()=> { Bicicleta.allBicis = [] });
describe('Bicicleta.allBicis',()=>{
	it('comienza vacio', ()=>{
		expect(Bicicleta.allBicis.length).toBe(0);
	});	
});


describe('Bicicleta.add',()=>{
	it('agregamnos una', ()=>{
		expect(Bicicleta.allBicis.length).toBe(0);
		var bici = new Bicicleta();
		Bicicleta.add(bici);
		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(bici);
	});	
});


describe('Bicicleta.findById',()=>{
	it('buscar por id 1', ()=>{
		expect(Bicicleta.allBicis.length).toBe(0);
		var bici1= new Bicicleta(1,"verde","bmx",[]);
		var bici2 = new Bicicleta(2,"amarillo","urbana",[]);
		Bicicleta.add(bici1);
		Bicicleta.add(bici2);
		
		var targetBici = Bicicleta.findById(1);
		expect(targetBici.id).toBe(1);
		expect(targetBici.color).toBe(bici1.color);
		expect(targetBici.modelo).toBe(bici1.modelo);
		//expect(Bicicleta.allBicis[0]).toBe(bici);
	});	
});


describe('Bicicleta.removeById',()=>{
	it('eliminar por id 1', ()=>{
		expect(Bicicleta.allBicis.length).toBe(0);
		var bici1= new Bicicleta(1,"verde","bmx",[]);
		var bici2 = new Bicicleta(2,"amarillo","urbana",[]);
		Bicicleta.add(bici1);
		Bicicleta.add(bici2);
		expect(Bicicleta.allBicis.length).toBe(2);
		Bicicleta.removeById(1);
		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(bici2);
		
		
	});
	
});
*/