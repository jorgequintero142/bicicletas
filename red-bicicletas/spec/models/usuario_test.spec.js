var mongoose = require("mongoose");
var Reserva = require('../../models/reserva');
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');

describe('Test Bicicletas',function(){
	beforeEach(function(done){
		
		var mongoDb = "mongodb://localhost:27017/testdb";
		mongoose.connect(mongoDb,{useNewUrlParser:true});
		const db = mongoose.connection;
		db.on('error test connect', console.error.bind(console, 'connection error:'));
		db.once('open', function() {
			console.log('Ok connect test ');
			done();
		}); 
	});	
	
afterEach(function(done){
	Reserva.deleteMany({},function(err,success){
		if (err) console.log(err);
		//mongoose.disconnect(err); 
	
			Usuario.deleteMany({},function(err,success){
				if (err) console.log(err);
					Bicicleta.deleteMany({},function(err,success){
					if (err) console.log(err);
						mongoose.disconnect(err); 
						done();
					});				
			});
		});		
	});	
	
		describe('Cuando un usuario reserva una bici',()=>{
		it('debe existir la reserva', (done)=>{
			const usuario = new Usuario({nombre:"Jorge"});
			usuario.save();
			
			const aBici = new Bicicleta({code:1, color:"verde",modelo:"urbana"});
			aBici.save();
			
			var hoy = new Date();
			var manana = new Date();
			manana.setDate(hoy.getDate()+1);
		
			
			usuario.reservar(aBici.id,hoy, manana, function(err,reserva){
				console.log(manana);
		
				Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err,reservas){
					if (err) console.log(err);
					console.log(reservas[0]);
					expect(reservas.length).toBe(1);
					expect(reservas[0].bicicleta.color).toBe("verde");
					expect(reservas[0].bicicleta.code).toBe(1);
					expect(reservas[0].usuario.nombre).toBe("Jorge");
					done();
					
				});
				
			});
			
			});
		
		});
	
});
