var mongoose = require("mongoose");
var Bicicleta = require('../../models/bicicleta');

describe('Test Bicicletas',function(){
	beforeEach(function(done){
		
		var mongoDb = "mongodb://localhost:27017/testdb";
		mongoose.connect(mongoDb,{useNewUrlParser:true});
		const db = mongoose.connection;
		db.on('error test connect', console.error.bind(console, 'connection error:'));
		db.once('open', function() {
			console.log('Ok connect test ');
			done();
		}); 
	});	
	
	afterEach(function(done){
		//console.log('borro ');
		Bicicleta.deleteMany({},function(err,success){
			if (err) console.log(err);
			mongoose.disconnect(err); 
			done();
		});		
	});
	
	describe('Bicicleta.createInstance',()=>{
		it('crear instancia', ()=>{
			
			var bici = Bicicleta.createInstance(1,"verde","de calle",[-22.9, -43.2]);
			
			
			expect(bici.code).toBe(1);
			expect(bici.color).toBe("verde");
			expect(bici.modelo).toBe("de calle");
			//done();
		});	
	});
		
		describe('Bicicleta.allBicis',()=>{
		it('comienza vacia', (done)=>{
		
				Bicicleta.allBicis(function(err, bicis){
			//	if (err) console.log(err);
				expect(bicis.length).toBe(0);
				done();
				});
			});	
		});
		
		
		describe('Bicicleta.add',()=>{
		it('agregamnos una bicicleta', (done)=>{
			var aBici = new Bicicleta({code:1, color:"verde",modelo:"urbana"});
		
				Bicicleta.add(aBici,function(err, newBici){
					if (err) console.log(err);
						Bicicleta.allBicis(function(err, bicis){
						expect(bicis.length).toBe(1);
						expect(bicis[0].color).toBe("verde");
						done();
				});
					
					
					
				});
				
			});	
		});
		
		
		describe('Bicicleta.findByCode',()=>{
		it('devolver la bici', (done)=>{
			
			Bicicleta.allBicis(function(err,bicis){
				expect(bicis.length).toBe(0);
				var aBici = new Bicicleta({code:1, color:"verde",modelo:"urbana"});
				Bicicleta.add(aBici,function(err, newBici){
					if (err) console.log(err);
						var aBici2 = new Bicicleta({code:1, color:"azul",modelo:"bmx"});
						Bicicleta.add(aBici,function(err, newBici){
							if (err) console.log(err);
							Bicicleta.findByCode(1,function(err,targetBici){
								expect(targetBici.color).toBe("verde");
								expect(targetBici.modelo).toBe("urbana");
								expect(targetBici.code).toBe(1);
								done();
							});
								
						});
				});
			});	
			
		});
});
	

	
});