var mymap = L.map('main_map').setView([-22.955, -43.182], 13);



L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoianF1aW50ZXJvODYiLCJhIjoiY2t1dW44aGZ3NjBjYTJ3bnppbWRrYjhkcyJ9.MuWxxIejvpgj0QUmISKrnw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);



 //L.marker([-22.934994204732774, -43.21347005669205]).addTo(mymap);
 //L.marker([-22.96976978377371, -43.17982442800912]).addTo(mymap);
 //L.marker([-22.97040198840331, -43.203170373560624]).addTo(mymap);
$.ajax({
	dataType: 'json',
	url : 'api/bicicletas',
	success: function(result){
		console.log(result);
		result.bicicletas.forEach(function(bici){
			 L.marker(bici.ubicacion,{title:bici.id}).addTo(mymap);
			
		});
	}	
	
});