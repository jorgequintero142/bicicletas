var mongoose = require("mongoose");
var Reserva = require("./reserva");
const uniqueValidator =  require('mongoose-unique-validator');
const bcrypt = require("bcrypt");
const crypto = require("crypto");
var Schema = mongoose.Schema;

const Token = require("../models/token");
const mailer = require("../mailer/mailer");


saltRounds = 10;


const validateEmail = function (email) {
	const re = "/^((?!\.)[\w\-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/";
	return re.match(email);
}
var usuarioSchema = new Schema({
	nombre:  {
		type: String, 
		trim:true, 
		required: [true, "El nombre es requerido"] 
		}, 
	email: {
		type: String, 
		trim:true, 
		required: [true, "El email es obligatorio"] ,
		lowercase: true,
		unique:true,
		validate:[validateEmail,'Por favor ingrese un email valido']
		,	match:"/^((?!\.)[\w\-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$/"
		},
	password : {
		type: String, 
		required: [true, "El password es obligatorio"]
		},
	passwordResetToken: String,
	passwordResetTokenExpires: Date,
	verificado: {
		type:Boolean,
		default:false
	}
});



usuarioSchema.plugin(uniqueValidator,{message:'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save',function(next){
	if (this.isModified('password')) {
		this.password = bcrypt.hashSync(this.password,saltRounds);
	}
	next();
});

usuarioSchema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password,this.password);
};
usuarioSchema.statics.add = function(aUsu,cb) {
	this.create(aUsu, cb);
};

usuarioSchema.statics.allUsers = function(cb) {
	return this.find({},cb);
};

usuarioSchema.methods.reservar = function(biciId,desde,hasta,cb){
	var reserva = new Reserva({usuario:this._id,bicicleta:biciId, desde:desde, hasta:hasta});
	console.log(reserva);	
	reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
	const token = new Token({usuario:this._id,token: crypto.randomBytes(16).toString('hex')});
	const email_destination = this.email;
	token.save(function(err) {
		if(err) {
			console.log(err);
		} 
		const mailOptions = {
			from:"no-reply@misbicis.com",
			to: email_destination,
			subject: "Verificacion de cuenta",
			text: "Por favor verificar su email haciendo click en http://127.0.0.1:3000/token/confirmacion/"+token.token
			
		};

		mailer.sendEmail(mailOptions);
		
		

		
		

		
	});
};


usuarioSchema.methods.enviar_email_forgot = function(mailOptions,cb) {
       mailer.sendEmail(mailOptions);
};
module.exports = mongoose.model("Usuario",usuarioSchema);