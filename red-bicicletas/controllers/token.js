var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
 confirmationGet: function(req, res, next){
	 Token.findOne({		 token:req.params.token	 },function(err, token){
		 if (!token) {
			 return res.status(400).send({type:'not-verified', msg:'No se encontro este token'});
		 }

		Usuario.findById(token.usuario,function(err,user){
			if (err) {
				console.log(err);
				return res.status(400).send({msg:'Error consultando el usuario'});
			}
			if(!user) {
				return res.status(400).send({msg:'No se encontro el usuario con este token'});
			}
			if(user.verificado) {
				res.redirect('/');
			}
			
			user.verificado = true;
			user.save(function(err){
				if(err) {  
					res.status(500).send({msg:err.message});
				} 
				res.redirect('/');
				
			});
		}); 
	 });
	 
 }	
	
}