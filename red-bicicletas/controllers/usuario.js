var Usuario = require('../models/usuario');





exports.create = function(req, res) {
	//res.render('usuarios/create',{});	
}

module.exports = {
	list: function(req,res,next) {
		Usuario.find({},(err,usuarios)=>{
			res.render('usuarios/index',{usuarios:usuarios});
		});
	},
	update_get: function(req,res,next) {
		Usuario.findById(req.params.id, function(err,usuario) {
			res.render('usuarios/update',{errors:{}, usuario:usuario});
		});
		
	},
	update: function(req,res,next) {
		var update_values = {nombre:req.body.nombre};
		Usuario.findByIdAndUpdate(req.params.id, update_values,function(err,usuario){
			if (err) {
				console.log(err);
				res.render('usuarios/update',{errors:err.errors, usuario:new Usuario()});
			} else {
				res.redirect('/usuarios');			}
				
		});
	},

	create_get: function(req, res, next) {
		res.render('usuarios/create', {errors:{},usuario : new Usuario()});
	},
	create: function(req, res, next) {
	console.log("Hola...");
	var usuario = new Usuario();
	usuario.nombre = req.body.nombre;
	usuario.email = req.body.email;
	
		if (req.body.password != req.body.passwordconfirm) {
			res.render('usuarios/create', {errors:{passwordconfirm : {message:"Password no son iguales"}}, usuario:usuario});
		} 
		
			Usuario.create({nombre:req.body.nombre, email:req.body.email,password:req.body.password},function(err,nuevoUsario){
			if (err) {
				res.render('usuarios/create', {errors:err.errors, usuario: new Usuario()});
				
			}
			else {
				nuevoUsario.enviar_email_bienvenida();
				res.redirect('/usuarios');	
			}
			}
			);
		
	
	}
}