var Usuario = require('../models/usuario');
const crypto = require("crypto");

module.exports.forgotPassword = function(req, res) {
	Usuario.findOne({ email: req.body.mail }, function (err, user) {
		if(err) {
			console.log("error "+err);
		}
		  if (!user) {
			  console.log("No se ha encontrado el usuario");
			  res.render('session/forgotpassword',{info: 'No se encontro usuario con el email '+req.body.mail });
			  //res.render('session/forgotpassword',{info:''});
		  }
		  var newPassword = crypto.randomBytes(2).toString('hex')
		  user.password = newPassword;		  
		  user.save(function(err){
			if(err) {  
					res.render('session/forgotpassword',{info: err});
			} 
			
			const mailOptions = {
				from:"no-reply@misbicis.com",
				to: req.body.mail,
				subject: "Recuperacion de cuenta",
				text: "La nueva contraseña es "+newPassword 
			};
			user.enviar_email_forgot(mailOptions);
		
			
			res.render('session/forgotpassword',{info:'Se ha enviado email con nueva contraseña'});
				
			});
	  
		});
		
	}