var Bicicleta = require('../../models/bicicleta');
exports.bicicleta_list = function(req, res) {
	Bicicleta.allBicis(function(err, bicis){
							res.status(200).json({bicicletas:bicis});
					});
}


exports.verBici = function(req, res) {
	console.log(req.body.id);
	Bicicleta.findByCode(req.body.id,function(err, bici) {
		if (err) {
				res.status(200).json({error: err});
		} else {
				res.status(200).json({bicicleta: bici});
		}
		
		
	});
	
}

exports.bicicleta_create = function(req, res) {
	var bici = new Bicicleta({code: req.body.code,	color:req.body.color,	modelo: req.body.modelo}); 
	Bicicleta.add(bici);
	res.status(200).json({bicicleta: bici});
	// 
//	bici.ubicacion = [req.body.lat,req.body.lng];
//	Bicicleta.add(bici);
	//res.status(200).json({bicicleta: bici});
}


exports.bicicleta_delete = function(req, res) {
	
	Bicicleta.removeByCode(req.body.id); 
	res.status(204).send();
}