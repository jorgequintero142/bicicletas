var Usuario = require('../../models/usuario');
var jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");


module.exports =  {autenticate : function(req,res, next){
	Usuario.findOne({email:req.body.email}, function(err, userInfo) {
		if(err) {
			next(err);
		} else {
			if(userInfo==null) {
				return res.status(401).json({status:"Error", message:"No se ha encontrado usuario", data:null});
			}
			if(userInfo!=null && bcrypt.compareSync(req.body.password,userInfo.password)) {
				
				const token = jwt.sign({id:userInfo._id},req.app.get('secretKey'), {expiresIn:'7d'});
				res.status(200).json({message:"usuario encontrado", data:{usuario:userInfo, token:token}});
				
				
			} else {
				res.status(401).json({status:"Error", message:"Invalid user/password"});
			}
			
		}
		
	});
	
}},  
	{
		forgotPassword:function(req,res,next){
		Usuario.findOne({ email: req.body.mail }, function (err, user) {
		if(err) {
			res.status(401).json({status:"Error", message:"Se ha presentado un error", data:null});
		}
		  if (!user) {
			res.status(401).json({status:"Error", message:"No se ha encontrado usuario", data:null});
		  }
		  var newPassword = crypto.randomBytes(2).toString('hex')
		  user.password = newPassword;		  
		  user.save(function(err){
			if(err) {  
				res.status(401).json({status:"Error", message:"Se ha presentado un error", data:null});
			} 
			
			const mailOptions = {
				from:"no-reply@misbicis.com",
				to: req.body.mail,
				subject: "Recuperacion de cuenta",
				text: "La nueva contraseña es "+newPassword 
			};
			user.enviar_email_forgot(mailOptions);
			res.status(200).json({status:"Error", message:"Se ha enviado email con nueva contraseña", data:null});
	
				
			});
	  
		});
	
		}
	};