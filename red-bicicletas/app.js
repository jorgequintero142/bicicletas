var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var passport = require('./config/passport');
var LocalStrategy = require('passport-local').Strategy;
var bodyParser = require("body-parser");
var jwt = require('jsonwebtoken');

var flash = require('connect-flash');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasApiRouter = require('./routes/api/bicicletas');
var usuariosApiRouter = require('./routes/api/usuarios');
var tokenRouter = require('./routes/token');
var authRouter = require('./routes/api/auth');
var loginRouter = require('./routes/login');

var store = new session.MemoryStore;
var app = express();


var mongoose = require('mongoose');
var mongoDb = "mongodb://localhost:27017/red_bicicletas";
mongoose.connect(mongoDb,{useNewUrlParser:true});
mongoose.Promise = global.Promise;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error MongoDB'));
db.once('open', function() {
	console.log('Ok connect');
}); 

app.set('secretKey','misBic!!iclet$#as123');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(flash());		
app.use(session({
	cookie:{maxAge: 24*60*60*1000},
	store:store,		
	saveUninitialized: true,
	resave: true,
	secret:'cats'
	}));	
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));




app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletas', validarUsuario,bicicletasApiRouter);
app.use('/api/usuarios', usuariosApiRouter);
app.use('/token', tokenRouter);
app.use('/api/auth', authRouter);


app.get('/login',function(req,res,next){
	res.render('session/login',{});	
	
})

app.get('/logout',function(req,res){
		req.logout();
		res.redirect('/');
});


app.use('/forgotPassword', loginRouter);

app.post('/login',function(req,res,next) {
	passport.authenticate('local',function(err,user,informacion){
		console.log(informacion);
		if (err) {
			return res.render('session/login',{info:err});
		}
		if (!user)  {
			return res.render('session/login',{info:informacion.message}); 
		}
		req.logIn(user,function(err) {
		if(err) {
			return next(err);
		}
			return res.redirect('/');
		});
	})(req,res,next);
});		





// catch 404 and forward to error handler
app.use(function(req, res, next) {
	next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function validarUsuario(req,res,next) {
	jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err,decoded){
		if(err) {
			res.json({status:"error",message:err.message, data:null});
		} else {
			req.body.userId =decoded.id;
		}
		console.log('jwt veriofy '+decoded);
		next();
	});
	
};

function loggedIn(req,res,next){
	console.log("el usuario es:");
	console.log(req.user);
	console.log("********");
	if(req.user) {
		next();
	} else {
		res.redirect('/login');
	}
	
};
  
module.exports = app;
