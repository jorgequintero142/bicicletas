var express = require('express');
var router = express.Router();
var loginController =  require('../controllers/login');



router.get('/forgotPassword',function(req,res){
		res.render('session/forgotpassword',{info:''});
});

router.post('/forgotPassword', loginController.forgotPassword);


module.exports = router;
