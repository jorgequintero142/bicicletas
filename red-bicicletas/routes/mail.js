var express = require('express');
var router = express.Router();
var mailController =  require('../controllers/nodemailerCtl')

router.get('/send', mailController.sendEmail);
module.exports = router;
