var express = require('express');
var router = express.Router();
var usuarioController =  require('../controllers/usuario');


router.get('/', usuarioController.list);
router.get('/create', usuarioController.create_get);
router.post('/create', usuarioController.create);
//router.get('/', usuarioController.list);
module.exports = router;
